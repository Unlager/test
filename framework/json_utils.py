import json


class JsonUtils:
    @staticmethod
    def is_json(response):
        try:
            response.json()
            return True
        except json.JSONDecodeError:
            return False
