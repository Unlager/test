from framework.apiutils import APIUtils
from config.urls import Urls
from framework.objects.post_object import Post
from framework.objects.user_object import User


class ProjectApiMethods:
    @staticmethod
    def get_all_users():
        request_text = Urls.URL + Urls.USERS
        response = APIUtils.send_get_request(request_text)
        data = response.json()
        if len(data) != 0:
            users = []
            for user_number in range(0, len(data)):
                users.append(
                    User(data[user_number]))
            return users, response.status_code, response
        return '', response.status_code, response

    @staticmethod
    def create_post(title, body, userId):
        request_text = Urls.URL + Urls.POSTS
        body_dict = {'userId': userId,
                     'title': title,
                     'body': body}
        response = APIUtils.send_post_request(request_text, body_dict=body_dict)
        data = response.json()
        if len(data) != 0:
            post = Post(data['userId'], data['id'], data['title'], data['body'])
            return post, response.status_code, response
        return '', response.status_code, response

    @staticmethod
    def get_all_posts():
        request_text = Urls.URL + Urls.POSTS
        response = APIUtils.send_get_request(request_text)
        data = response.json()
        if len(data) != 0:
            posts = []
            for post_number in range(0, len(data)):
                posts.append(
                    Post(data[post_number]['userId'],
                         data[post_number]['id'],
                         data[post_number]['title'],
                         data[post_number]['body']))
            return posts, response.status_code, response
        return '', response.status_code, response

    @staticmethod
    def get_post_by_id(post_id):
        request_text = Urls.URL + Urls.POSTS + str(post_id)
        response = APIUtils.send_get_request(request_text)
        data = response.json()
        if len(data) != 0:
            post = Post(data['userId'], data['id'], data['title'], data['body'])
            return post, response.status_code, response
        return '', response.status_code, response
