<h1>Test case "REST API"</h1>
<ul>
    <li>
        <h3>Настройка окружения</h3>
        <p>Использовался python 3.9</p>
        <p>Файл <b>requirements.txt</b> содержит подключенные библиотеки</p>
        <p>Установка: <code>pip install -r requirements.txt</code></p>
    </li> 
    <li>
        <h3>Запуск</h3>
        <p>В основной директории проекта выполнить<code>pytest</code></p>
    </li>
    <li>
        <h3>Навигация по проекту</h3>
        <h4>Основная директория</h4>
        <p><i>testData.json</i> - данные для тестирования в формате json</p>
        <h4>/config</h4>
        <p><i>urls.py</i> - класс с paths</p>
        <h4>/framework</h4>
        <p><i>apiutils.py</i> - класс работы с API</p>
        <p><i>json_utils.py</i> - проверка на json формат</p>
        <p><i>project_api_methods.py</i> - статические методы для работы с apiutils</p>
            <ul>
                <h4>/objects</h4>
                    <p><i>address_object.py</i> - класс описания объекта адрес</p>
                    <p><i>company_object.py</i> - класс описания объекта компании</p>
                    <p><i>geo_object.py</i> - класс описания объекта геопозиции</p>
                    <p><i>post_object.py</i> - класс описания объекта поста</p>
                    <p><i>user_object.py</i> - класс описания объекта пользователя</p>
            </ul>
        <h4>/tests</h4>
        <p><i>test_case.py</i> - Тест кейс</p>
        <h4>/utils</h4>
        <p><i>data_manager.py</i> - работа с данными</p>
        <p><i>text_generator.py</i> - генератор текста и пароля</p>
    </li>
 </ul>