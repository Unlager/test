class Geo:
    def __init__(self, data):
        self.lat = data['lat']
        self.lng = data['lng']

    def __eq__(self, other):
        if isinstance(other, Geo):
            return (self.lat == other.lat and
                    self.lng == other.lng)
        return NotImplemented
