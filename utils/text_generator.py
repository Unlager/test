from random import choice
from string import ascii_letters


class TextGegenerator:
    @staticmethod
    def simple_text(size):
        text = ''.join(choice(ascii_letters) for i in range(size))
        return text

    @staticmethod
    def simple_password(size):
        letters_and_numbers = ascii_letters + '1234567890'
        password = ''.join(choice(letters_and_numbers) for i in range(size))
        return password
