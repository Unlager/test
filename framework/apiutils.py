from json import JSONDecodeError
import requests
from requests.exceptions import HTTPError


class APIUtils:
    @staticmethod
    def send_get_request(request_text, header_dict=None):
        if request_text == '':
            return 'Error: The field is empty'

        try:
            response = requests.get(request_text, headers=header_dict)
            if response.text == '':
                return ''
            else:
                return response
        except HTTPError as http_err:
            return f'HTTP error occurred: {http_err}'
        except Exception as err:
            return f'Other error occurred: {err}'

    @staticmethod
    def send_post_request(request_text, header_dict=None, body_dict=None):
        if request_text == '':
            return 'Error: The field is empty'
        try:
            response = requests.post(request_text, headers=header_dict, json=body_dict)
            if response.text == '':
                return ''
            else:
                return response
        except HTTPError as http_err:
            return f'HTTP error occurred: {http_err}'
        except Exception as err:
            return f'Other error occurred: {err}'
