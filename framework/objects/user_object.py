from framework.objects.address_object import Address
from framework.objects.company_object import Company


class User:
    def __init__(self, data):
        self.name = data['name']
        self.username = data['username']
        self.email = data['email']
        self.phone = data['phone']
        self.website = data['website']
        self.address = Address(data['address'])
        self.company = Company(data['company'])

    def __eq__(self, other):
        if isinstance(other, User):
            return (self.name == other.name and
                    self.username == other.username and
                    self.email == other.email and
                    self.phone == other.phone and
                    self.address == other.address and
                    self.company == other.company and
                    self.website == other.website)
        return NotImplemented
