from utils.data_manager import DataManager
from utils.text_generator import TextGegenerator
from framework.json_utils import JsonUtils
from framework.objects.post_object import Post
from framework.objects.user_object import User
from framework.project_api_methods import ProjectApiMethods as PAM
import pytest


class TestCase:

    def test_send_request_for_get_specific_post(self):
        test_data = DataManager.get_test_data("data_case_2")
        expStatusCode = test_data['status_code']
        expPost = Post(test_data['userId'], test_data['id'], test_data['title'], test_data['body'])
        actPost, actStatusCode, response = PAM.get_post_by_id(test_data['request'])
        assert JsonUtils.is_json(response), 'Response has not json format'
        assert actPost != expPost, 'Objects is not equality'
        assert actStatusCode == expStatusCode, 'Status code is not correct'

    def test_send_request_for_get_empty(self):
        test_data = DataManager.get_test_data("data_case_3")
        expPost, expStatusCode = '', test_data['status_code']
        actPost, actStatusCode, response = PAM.get_post_by_id(test_data['request'])
        assert JsonUtils.is_json(response), 'Response has not json format'
        assert actPost == expPost, 'Answer is not empty'
        assert actStatusCode == expStatusCode, 'Status code is not correct'

    def test_check_sort_of_posts(self):
        test_data = DataManager.get_test_data("data_case_1")
        expStatusCode = test_data['status_code']
        actPosts, actStatusCode, response = PAM.get_all_posts()
        assert JsonUtils.is_json(response), 'Response has not json format'
        for i in range(len(actPosts) - 2):
            assert int(actPosts[i].userId) <= int(actPosts[i + 1].userId), 'Sorted wrong'
        assert actStatusCode == expStatusCode, 'Status code is not correct'

    def test_send_request_for_create_post(self):
        test_data = DataManager.get_test_data("data_case_4")
        expStatusCode = test_data['status_code']
        title = TextGegenerator.simple_text(test_data['title_size'])
        body = TextGegenerator.simple_text(test_data['body_size'])
        expPost = Post(test_data['userId'], test_data['id'], title, body)
        actPost, actStatusCode, response = PAM.create_post(title, body, test_data['userId'])
        assert JsonUtils.is_json(response), 'Response has not json format'
        assert actPost == expPost, 'Objects is not equality'
        assert actStatusCode == expStatusCode, 'Status code is not correct'

    def test_send_request_for_get_users(self):
        test_data = DataManager.get_test_data("data_case_5")
        actUsers, actStatusCode, response = PAM.get_all_users()
        assert JsonUtils.is_json(response), 'Response has not json format'
        expUser, expStatusCode = User(test_data['data']), test_data['status_code']
        actUser = actUsers[test_data['id'] - 1]
        assert actUser == expUser, 'Data is not correct'
        DataManager().add_data(actUser)
        assert actStatusCode == expStatusCode, 'Status code is not correct'

    def test_send_request_for_get_specific_user(self):
        test_data = DataManager.get_test_data("data_case_6")
        expStatusCode = test_data['status_code']
        actUsers, actStatusCode, response = PAM.get_all_users()
        assert JsonUtils.is_json(response), 'Response has not json format'
        actUser = actUsers[test_data['id'] - 1]
        user_from_storage = DataManager().get_data_of(0)
        assert actUser == user_from_storage, 'User is not correct'
        assert actStatusCode == expStatusCode, 'Status code is not correct'
