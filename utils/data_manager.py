import pathlib
from pathlib import Path
import json


class DataManager:
    __instance = None
    storage = []

    def add_data(self, value):
        self.storage.append(value)

    def clear_data(self):
        self.storage.clear()

    def get_all_data(self):
        return self.storage

    def get_data_of(self, value):
        return self.storage[value]

    def get_len_data(self):
        return len(self.storage)

    @staticmethod
    def get_test_data(value):
        path = Path(pathlib.Path.cwd(), "testData.json")
        with open(path, "r") as read_file:
            data = json.load(read_file)
        data = data[value]
        return data

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super(DataManager, cls).__new__(cls)
        return cls.__instance
