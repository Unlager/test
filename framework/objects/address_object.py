from framework.objects.geo_object import Geo


class Address:
    def __init__(self, data):
        self.street = data['street']
        self.suite = data['suite']
        self.city = data['city']
        self.zipcode = data['zipcode']
        self.geo = Geo(data['geo'])

    def __eq__(self, other):
        if isinstance(other, Address):
            return (self.street == other.street and
                    self.suite == other.suite and
                    self.city == other.city and
                    self.zipcode == other.zipcode and
                    self.geo == other.geo)
        return NotImplemented
