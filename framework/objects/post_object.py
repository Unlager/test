class Post:
    def __init__(self, userId, post_id, title, body):
        self.userId = userId
        self.post_id = post_id
        self.title = title
        self.body = body

    def __eq__(self, other):
        if isinstance(other, Post):
            return (self.userId == other.userId and
                    self.post_id == other.post_id and
                    self.title == other.title and
                    self.body == other.body)
        return NotImplemented


