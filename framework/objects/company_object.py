class Company:
    def __init__(self, data):
        self.name = data['name']
        self.catchPhrase = data['catchPhrase']
        self.bs = data['bs']

    def __eq__(self, other):
        if isinstance(other, Company):
            return (self.name == other.name and
                    self.catchPhrase == other.catchPhrase and
                    self.bs == other.bs)
        return NotImplemented
